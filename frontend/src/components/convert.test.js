import {convertTemp} from './convert';

describe('Kelvin to Fahrenheit', () => {
  it('Handle unknown unit', () => {
    const expected = {val:'--',unit:'--'};
    const actual = convertTemp({val:295,unit:'unknown-unit'},'f');
    expect(actual).toEqual(expected);
  });
  it('Handle invalid from value', () => {
    const expected = {val:'--',unit:'--'};
    const actual = convertTemp({val:'invalid',unit:'k'},'f');
    expect(actual).toEqual(expected);
  });
  it('Handle null', () => {
    const expected = {val:'--',unit:'--'};
    const actual = convertTemp(null,'f');
    expect(actual).toEqual(expected);
  });
  it('Handle invalid to value', () => {
    const expected = {val:'--',unit:'--'};
    const actual = convertTemp({val:295,unit:'k'},'invalid');
    expect(actual).toEqual(expected);
  });
  it('Handle null to value', () => {
    const expected = {val:'--',unit:'--'};
    const actual = convertTemp({val:295,unit:'k'},null);
    expect(actual).toEqual(expected);
  });
  it('Handle Fahrenheit', () => {
    const expected = {val:71.33,unit:'f'};
    const actual = convertTemp({val:295,unit:'k'},'f');
    expect(actual).toEqual(expected);
  });
  it('Handle Fahrenheit', () => {
    const expected = {val:62.33,unit:'f'};
    const actual = convertTemp({val:290,unit:'k'},'f');
    expect(actual).toEqual(expected);
  });
  it('Handle Celcius', () => {
    const expected = {val:21.85,unit:'c'};
    const actual = convertTemp({val:295,unit:'k'},'c');
    expect(actual).toEqual(expected);
  });
  it('Handle Celcius', () => {
    const expected = {val:16.85,unit:'c'};
    const actual = convertTemp({val:290,unit:'k'},'c');
    expect(actual).toEqual(expected);
  });
});