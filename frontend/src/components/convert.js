
module.exports = {
  convertTemp: (from, toUnit) => {
    if(from && from.unit === 'k'){
      // Support converting from Kelvin
      if(typeof from.val === 'number'){
        if(toUnit == 'f'){
          // Convert to Fahrenheit, round to 2 decimal places, convert back to type number
          const val = +(from.val * 9/5 - 459.67).toFixed(2);
          return {val, unit:'f'};
        }else if(toUnit == 'c'){
          // Convert to Celcius, round to 2 decimal places, convert back to type number
          const val = +(from.val - 273.15).toFixed(2);
          return {val, unit:'c'};
        }
      }
    }
    return {val:'--', unit:'--'};
    
  }
}