import moment from 'moment';
module.exports = function splitForecast(forecastData){
  if(!forecastData){return;}
  return forecastData.reduce((accumulator, current) => {
    if(current && current.date){
      const key = moment(current.date).format('M/D/YYYY');
      if(accumulator[key]){
        accumulator[key].push(current);
      }else{
        accumulator[key] = [current];
      }
    }
    return accumulator;
  },{});
}