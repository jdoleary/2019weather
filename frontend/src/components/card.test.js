import React from 'react';
import { shallow} from 'enzyme';

import Card from './Card';


describe('Props', () => {
  const date = '2017-02-01 03:00:00';
  const temp = {val: 72, unit: 'f'};
  const alt = 'light rain';
  const icon = '10d';
  let wrapper;
  beforeAll(()=>{
    wrapper = shallow(<Card 
      date={date}
      temp={temp}
      alt={alt}
      icon={icon}
      />);

  });
  describe('Props', () => {
    it('Should render image src properly', () => {
      const expectedIcon = `https://openweathermap.org/img/w/${icon}.png`;
      expect(wrapper.find('.icon').prop('src')).toEqual(expectedIcon);
    });
    it('Should render image alt properly', () => {
      expect(wrapper.find('.icon').prop('alt')).toEqual(alt);
    });
    it('Should render temp properly', () => {
      expect(wrapper.find('.temp').text()).toEqual(`${temp.val} ${temp.unit}°`);
    });
    it('Should render time properly', () => {
      const formattedTime = date ? new Date(date).toLocaleTimeString() : '';
      expect(wrapper.find('.time').text()).toEqual(formattedTime);
    });
    
  });
});

