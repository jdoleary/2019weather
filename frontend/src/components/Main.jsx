import * as React from 'react';
import axios from 'axios';
import Card from './Card';
import {convertTemp} from './convert';
import splitForecast from './splitForecast';


export default class Main extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      showFiveDay: false,
      usingGeolocation: false,
      lat:40.767318,
      lon:-73.970496,
      isFahrenheight: true,
      today:{},
      forecast:{},
      error:null,
      loading:true
    }
    this.gotData = this.gotData.bind(this);
    this.renderCard = this.renderCard.bind(this);
    this.renderForecast = this.renderForecast.bind(this);
    this.handleError = this.handleError.bind(this);
  }
  componentDidMount(){
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        position => {
          this.setState({usingGeolocation:true});
          if(position.coords){
            axios.get(`/api?lat=${position.coords.latitude}&lon=${position.coords.longitude}`)
              .then(this.gotData)
              .catch(this.handleError);
          }
        });
    }
  }
  handleError(error){
    this.setState({error});
  }
  gotData(response){
    const {data} = response;
    if(!data){return;}
    const groupedForecast = splitForecast(data.forecast);
    this.setState({
      loading:false,
      today:data.today,
      forecast:groupedForecast,
      error:null
    });
  }
  renderForecast(groupedForecastData){
    let columns = [];
    for (const key of Object.keys(groupedForecastData)) {
      columns.push(
        <div className="column" key={key}>
          <div className="date">{key}</div>
          {groupedForecastData[key].map(this.renderCard)}
        </div>);
    }
    return columns;
  }
  renderCard(cardData, key){
    return <Card key={key} date={cardData.date} icon={cardData.icon} temp={convertTemp({val:cardData.val,unit:cardData.unit}, this.state.isFahrenheight ? 'f' : 'c')}/>
  }
  render() {
    if(this.state.error){
      return <div>Something went wrong!  Please try again later.</div>;
    }
    if(!this.state.usingGeolocation){
      return <div>Please enable GeoLocation on your browser to see the weather in your current location</div>;
    }
    if(this.state.loading){
      return <div>Loading local weather data...</div>;
    }
    const page = this.state.showFiveDay
      ? (<div className="columns">{this.state.forecast && this.renderForecast(this.state.forecast)}</div>)
      : (this.state.today && (<div className="column">Today {this.renderCard(this.state.today, 'card-today')}</div>));

    return <main>
      <button onClick={()=>this.setState({showFiveDay:!this.state.showFiveDay})}>
        {this.state.showFiveDay ? 'Switch to Current Day Weather' : 'Switch to Five Day Temperatures'}
      </button>
      <button onClick={()=>this.setState({isFahrenheight:!this.state.isFahrenheight})}>{this.state.isFahrenheight ? 'Switch to Celcius' : 'Switch to Fahrenheight'}</button>
      {page}
    </main>;
  }
}