import * as React from "react";
import moment from 'moment';

export default class Card extends React.Component {
  render() {
    const {
      date,
      temp,
      alt,
      icon
    } = this.props;
    const formattedTime = date ? moment(date).format('h:mm:ss A') : '';
    return <div className="card">
        {formattedTime && <div className="time">{formattedTime}</div>}
        <img className="icon" src={`/${icon}.png`} alt={alt} />
        <div className="temp">{temp.val} {temp.unit}&deg;</div>
      </div>;
  }
}

Card.defaultProps = {
  alt: 'Weather Icon'
}