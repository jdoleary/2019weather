
# Jordan O'Leary Sample Weather App

This is a sample weather app.
The backend serves the frontend HTML and severs an api endpoint for the frontend to make requests to.

# View on Heroku
Check out the below link to see the app live.  (Note: This is a free heroku account, so if it doesn't respond right away, try refreshing the page after a few minutes so that Heroku can spin up an instance to serve the app)

https://jordan-oleary-weather.herokuapp.com/

# Build it yourself

NOTE: The .env file that contains the api key is not included in the repository as is standard security practice.

To build and run the app yourself, complete the following steps in order:
- Create a .env file in `backend/` with the following contents:

(Replace 123456 with a valid openweathermap API key)
```
API_KEY=123456
```

- Run `npm install` in both backend/ and frontend/

- Run `./build.sh` which will build the frontend and move it's built files into the backend directory to serve.

- Run `./runServer.sh` to start the server

- Visit `http://localhost:3000`