echo "Be sure you've run 'npm install' in both backend/ and frontend/"
rm -r backend/dist
rm -r frontend/dist
cd frontend && npm run build
cd ..
cp -r frontend/dist backend/dist
cp frontend/images/* backend/dist
