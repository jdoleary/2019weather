async function getWeatherData(httpClient, queryString, API_KEY){
  if(!queryString || !API_KEY){
    return {status:404, statusText:'Not Found'};
  }
  try{
    const today = `http://api.openweathermap.org/data/2.5/weather?${queryString}&APPID=${API_KEY}`;
    const forecast = `http://api.openweathermap.org/data/2.5/forecast?${queryString}&APPID=${API_KEY}`;
    const [todayResponse, forecastResponse] = await Promise.all([httpClient.get(today), httpClient.get(forecast)]);
    return {
      status:200,
      statusText:'OK',
      today:extractSingleDayWeatherData(todayResponse.data),
      forecast:extractForecastWeatherData(forecastResponse.data)
    };
  }catch(error){
    if(error.response){
      const {status, statusText} = error.response;
      throw {status, statusText};
    }else{
      throw {status:500};
    }
  }
}

function extractSingleDayWeatherData(data){
  if(!(data && data.main)){return;}
  return {
    val:data.main.temp,
    unit:'k',
    icon: data.weather && data.weather[0] && data.weather[0].icon
  } 
}

function extractForecastWeatherData(data){
  if(!(data && data.list)){return;}

  return data.list.map(entry => ({
    val:entry.main.temp,
    unit:'k',
    icon: entry.weather && entry.weather[0] && entry.weather[0].icon,
    date: entry.dt_txt
  }));
}

module.exports = {
  getWeatherData,
  testable: {
    extractForecastWeatherData,
    extractSingleDayWeatherData
  }
};