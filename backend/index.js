require('dotenv').config();
const serve = require('koa-static');
const mount = require('koa-mount');
const Koa = require('koa');
const axios = require('axios');
const {getWeatherData} = require('./request');
const PORT = process.env.PORT || 3000;
const {API_KEY} = process.env;

if(!API_KEY){
  console.log(`
  NOTICE: Due to security best practices, the api key is not included in the public repository.
  Add a file called ".env" in the same directory as index.js with the contents:
  API_KEY=[YOUR API KEY HERE]
  `);
}else{
  // Serve api endpoint:
  const koaApi = new Koa()
  koaApi.use(async function (ctx, next){
    await next();
    try{
      const response = await getWeatherData(axios,ctx.querystring,API_KEY);
      ctx.status = 200;
      ctx.body = response;
    }catch(e){
      ctx.status = 500;
    }
  });

  const app = new Koa();
  app.use(serve(`${__dirname}/dist`));
  app.use(mount('/api', koaApi));
  app.listen(PORT);
  console.log(`Backend started on port ${PORT}`);
}